# README #


### Purpose ###
This repository contains a Cactus thorn that demonstrates how to implement a set of coupled wave equations within Cactus. It is intended to be used as part of a tutorial.


### Downloading and Building ###

In the directory that contains your `Cactus` directory, run

`./GetComponents https://bitbucket.org/yosef_zlochower/tutorial/raw/tutorial_2023/config_files/tutorial.th`

A minimal build thornlist is provided in `config_files/ThornList`. You can also find a configuration file for CentOs8 there.

#### Building ####
1.  `gmake maxwell-config options=simfactory/mdb/optionlists/generic.cfg` 
2.  `cp repos/tutorial/config_files/ThornList configs/maxwell/` 
3.  `gmake maxwell` 
