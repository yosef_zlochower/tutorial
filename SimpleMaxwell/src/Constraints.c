#include <string.h>
#include <math.h>
#include <stdio.h>
#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "loopcontrol.h"
void SimpleMaxwell_Calc_Constraints(CCTK_ARGUMENTS);

void SimpleMaxwell_Calc_Constraints(CCTK_ARGUMENTS)
{
  // The following two macros ensure that our function can "see" the appropriate
  // gridfunctions and paramters.
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // dx, dy, and dz are the gridspacings in the x, y, and z directions
  const CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz = CCTK_DELTA_SPACE(2);

  const int ni = cctkGH->cctk_lsh[0];                   
  const int nj = cctkGH->cctk_lsh[1];
  const int nk = cctkGH->cctk_lsh[2];
                                              
  const int ghost_x = cctkGH->cctk_nghostzones[0];
  const int ghost_y = cctkGH->cctk_nghostzones[1]; 
  const int ghost_z = cctkGH->cctk_nghostzones[2];

# ifdef _OPENMP
#   pragma omp parallel
# endif	
  LC_LOOP3(Constraint_Maxwell, i, j, k,
       ghost_x, ghost_y, ghost_z,
       ni - ghost_x, nj - ghost_y, nk - ghost_z,
       ni, nj, nk)
  {
        const int ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);
        const int ip1jk = CCTK_GFINDEX3D(cctkGH, i + 1, j, k);
        const int im1jk = CCTK_GFINDEX3D(cctkGH, i - 1, j, k);
        const int ijp1k = CCTK_GFINDEX3D(cctkGH, i, j + 1, k);
        const int ijm1k = CCTK_GFINDEX3D(cctkGH, i, j - 1, k);
        const int ijkp1 = CCTK_GFINDEX3D(cctkGH, i, j, k + 1);
        const int ijkm1 = CCTK_GFINDEX3D(cctkGH, i, j, k - 1);

        const double dx_Bx =  0.5 * (Bx[ip1jk] - Bx[im1jk]) / dx ;
        const double dy_By =  0.5 * (By[ijp1k] - By[ijm1k]) / dy ;
        const double dz_Bz =  0.5 * (Bz[ijkp1] - Bz[ijkm1]) / dz ;

        const double dx_Dx =  0.5 * (Dx[ip1jk] - Dx[im1jk]) / dx ;
        const double dy_Dy =  0.5 * (Dy[ijp1k] - Dy[ijm1k]) / dy ;
        const double dz_Dz =  0.5 * (Dz[ijkp1] - Dz[ijkm1]) / dz ;

        cD[ijk] = dx_Dx + dy_Dy + dz_Dz;
        cB[ijk] = dx_Bx + dy_By + dz_Bz;
  }
  LC_ENDLOOP3(Constraint_Maxwell);
}
