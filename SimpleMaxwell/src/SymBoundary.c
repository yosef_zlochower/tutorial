#include "cctk.h"
#include <string.h>
#include <math.h>
#include <stdio.h>
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

void SimpleMaxwell_Boundaries(CCTK_ARGUMENTS);
void SimpleMaxwell_ConstraintBoundaries(CCTK_ARGUMENTS);

/* Tell the boundary thorn to schedule symmetry
 * boundaries for the evolved variables in this thorn
 */
void SimpleMaxwell_Boundaries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                            "SimpleMaxwell::EvolvedVectorD", "None");
  Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                            "SimpleMaxwell::EvolvedVectorB", "None");
  Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                            "SimpleMaxwell::EvolvedScalars", "None");
}

void SimpleMaxwell_ConstraintBoundaries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                            "SimpleMaxwell::EvolvedConstraintScalars", "Scalar");
}
