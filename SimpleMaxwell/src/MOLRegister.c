#include "cctk.h"
#include <string.h>
#include <math.h>
#include <stdio.h>
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

void SimpleMaxwell_MoLRegister(CCTK_ARGUMENTS);

/* Let the Method-Of-Lines thorn know which variables are
 * evolved and which variables contain the RHS for those variables
 */
void SimpleMaxwell_MoLRegister(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  int err = 0;
  err += MoLRegisterEvolved(CCTK_VarIndex("SimpleMaxwell::Dx"),
                            CCTK_VarIndex("SimpleMaxwell::dotDx"));
  err += MoLRegisterEvolved(CCTK_VarIndex("SimpleMaxwell::Dy"),
                            CCTK_VarIndex("SimpleMaxwell::dotDy"));
  err += MoLRegisterEvolved(CCTK_VarIndex("SimpleMaxwell::Dz"),
                            CCTK_VarIndex("SimpleMaxwell::dotDz"));
  err += MoLRegisterEvolved(CCTK_VarIndex("SimpleMaxwell::Bx"),
                            CCTK_VarIndex("SimpleMaxwell::dotBx"));
  err += MoLRegisterEvolved(CCTK_VarIndex("SimpleMaxwell::By"),
                            CCTK_VarIndex("SimpleMaxwell::dotBy"));
  err += MoLRegisterEvolved(CCTK_VarIndex("SimpleMaxwell::Bz"),
                            CCTK_VarIndex("SimpleMaxwell::dotBz"));
  err += MoLRegisterEvolved(CCTK_VarIndex("SimpleMaxwell::PsiB"),
                            CCTK_VarIndex("SimpleMaxwell::dotPsiB"));
  err += MoLRegisterEvolved(CCTK_VarIndex("SimpleMaxwell::PsiD"),
                            CCTK_VarIndex("SimpleMaxwell::dotPsiD"));

  err += MoLRegisterConstrained(CCTK_VarIndex("SimpleMaxwell::cD"));
  err += MoLRegisterConstrained(CCTK_VarIndex("SimpleMaxwell::cB"));

  if (err)
  {
    CCTK_WARN(CCTK_WARN_ABORT, "error registering evolution system");
  }
}
