#include "cctk.h"
#include <string.h>
#include <math.h>
#include <stdio.h>
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "loopcontrol.h"

void SimpleMaxwell_RHS_Interior(CCTK_ARGUMENTS);

void SimpleMaxwell_RHS_Interior(CCTK_ARGUMENTS)
{
  // The following two macros ensure that our function can "see" the appropriate gridfunctions
  // and paramters.
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // dx, dy, and dz are the gridspacings in the x, y, and z directions
  const CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz = CCTK_DELTA_SPACE(2);

  const int ni = cctkGH->cctk_lsh[0];                   
  const int nj = cctkGH->cctk_lsh[1];
  const int nk = cctkGH->cctk_lsh[2];
                                              
  const int ghost_x = cctkGH->cctk_nghostzones[0];
  const int ghost_y = cctkGH->cctk_nghostzones[1]; 
  const int ghost_z = cctkGH->cctk_nghostzones[2];

# ifdef _OPENMP
#   pragma omp parallel
# endif	
  LC_LOOP3(Main_Maxwell, i, j, k,
       ghost_x, ghost_y, ghost_z,
       ni - ghost_x, nj - ghost_y, nk - ghost_z,
       ni, nj, nk)
  {
        const int ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);
        const int ip1jk = CCTK_GFINDEX3D(cctkGH, i + 1, j, k);
        const int im1jk = CCTK_GFINDEX3D(cctkGH, i - 1, j, k);
        const int ijp1k = CCTK_GFINDEX3D(cctkGH, i, j + 1, k);
        const int ijm1k = CCTK_GFINDEX3D(cctkGH, i, j - 1, k);
        const int ijkp1 = CCTK_GFINDEX3D(cctkGH, i, j, k + 1);
        const int ijkm1 = CCTK_GFINDEX3D(cctkGH, i, j, k - 1);

        const double dx_Bx =  0.5 * (Bx[ip1jk] - Bx[im1jk]) / dx;
        const double dy_Bx =  0.5 * (Bx[ijp1k] - Bx[ijm1k]) / dy;
        const double dz_Bx =  0.5 * (Bx[ijkp1] - Bx[ijkm1]) / dz;

        const double dx_By =  0.5 * (By[ip1jk] - By[im1jk]) / dx;
        const double dy_By =  0.5 * (By[ijp1k] - By[ijm1k]) / dy;
        const double dz_By =  0.5 * (By[ijkp1] - By[ijkm1]) / dz;

        const double dx_Bz =  0.5 * (Bz[ip1jk] - Bz[im1jk]) / dx;
        const double dy_Bz =  0.5 * (Bz[ijp1k] - Bz[ijm1k]) / dy;
        const double dz_Bz =  0.5 * (Bz[ijkp1] - Bz[ijkm1]) / dz;

        const double dx_Dx =  0.5 * (Dx[ip1jk] - Dx[im1jk]) / dx;
        const double dy_Dx =  0.5 * (Dx[ijp1k] - Dx[ijm1k]) / dy;
        const double dz_Dx =  0.5 * (Dx[ijkp1] - Dx[ijkm1]) / dz;

        const double dx_Dy =  0.5 * (Dy[ip1jk] - Dy[im1jk]) / dx;
        const double dy_Dy =  0.5 * (Dy[ijp1k] - Dy[ijm1k]) / dy;
        const double dz_Dy =  0.5 * (Dy[ijkp1] - Dy[ijkm1]) / dz;

        const double dx_Dz =  0.5 * (Dz[ip1jk] - Dz[im1jk]) / dx;
        const double dy_Dz =  0.5 * (Dz[ijp1k] - Dz[ijm1k]) / dy;
        const double dz_Dz =  0.5 * (Dz[ijkp1] - Dz[ijkm1]) / dz;

        const double dx_PsiB =  0.5 * (PsiB[ip1jk] - PsiB[im1jk]) / dx;
        const double dy_PsiB =  0.5 * (PsiB[ijp1k] - PsiB[ijm1k]) / dy;
        const double dz_PsiB =  0.5 * (PsiB[ijkp1] - PsiB[ijkm1]) / dz;

        const double dx_PsiD =  0.5 * (PsiD[ip1jk] - PsiD[im1jk]) / dx;
        const double dy_PsiD =  0.5 * (PsiD[ijp1k] - PsiD[ijm1k]) / dy;
        const double dz_PsiD =  0.5 * (PsiD[ijkp1] - PsiD[ijkm1]) / dz;

        const double dx_ieps =  0.5 * (ieps[ip1jk] - ieps[im1jk]) / dx;
        const double dy_ieps =  0.5 * (ieps[ijp1k] - ieps[ijm1k]) / dy;
        const double dz_ieps =  0.5 * (ieps[ijkp1] - ieps[ijkm1]) / dz;

        const double dx_imu =  0.5 * (imu[ip1jk] - imu[im1jk]) / dx;
        const double dy_imu =  0.5 * (imu[ijp1k] - imu[ijm1k]) / dy;
        const double dz_imu =  0.5 * (imu[ijkp1] - imu[ijkm1]) / dz;


        dotBx[ijk] = dx_PsiB - dy_Dz*ieps[ijk] - dy_ieps*Dz[ijk] + dz_Dy*ieps[ijk] + dz_ieps*Dy[ijk];
        dotBy[ijk] = dx_Dz*ieps[ijk] + dx_ieps*Dz[ijk] + dy_PsiB - dz_Dx*ieps[ijk] - dz_ieps*Dx[ijk];
        dotBz[ijk] = -dx_Dy*ieps[ijk] - dx_ieps*Dy[ijk] + dy_Dx*ieps[ijk] + dy_ieps*Dx[ijk] + dz_PsiB;
        
        dotDx[ijk] = dx_PsiD + dy_Bz*imu[ijk] + dy_imu*Bz[ijk] - dz_By*imu[ijk] - dz_imu*By[ijk];
        dotDy[ijk] = -dx_Bz*imu[ijk] - dx_imu*Bz[ijk] + dy_PsiD + dz_Bx*imu[ijk] + dz_imu*Bx[ijk];
        dotDz[ijk] = dx_By*imu[ijk] + dx_imu*By[ijk] - dy_Bx*imu[ijk] - dy_imu*Bx[ijk] + dz_PsiD;
        
        dotPsiB[ijk] = dx_Bx + dy_By + dz_Bz - kappa_B*PsiB[ijk];
        dotPsiD[ijk] = dx_Dx + dy_Dy + dz_Dz - kappa_D*PsiD[ijk];

  }
  LC_ENDLOOP3(Main_Maxwell);
}
