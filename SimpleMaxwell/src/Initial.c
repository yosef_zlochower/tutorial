#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "loopcontrol.h"

void SimpleMaxwell_Initialize(CCTK_ARGUMENTS);
void SimpleMaxwell_SetConstantFields(CCTK_ARGUMENTS);

static void SimpleMaxwell_ConstantMaterial(CCTK_ARGUMENTS);
static void SimpleMaxwell_RandomNoise(CCTK_ARGUMENTS);
static void SimpleMaxwell_Gaussian(CCTK_ARGUMENTS);
static void SimpleMaxwell_Pulse(CCTK_ARGUMENTS);

void SimpleMaxwell_Initialize(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (CCTK_Equals(initial_data, "compact_pulse_plus_z") ||
      CCTK_Equals(initial_data, "compact_pulse_bidirectional"))
     
  {
    SimpleMaxwell_Pulse(CCTK_PASS_CTOC);
  }
  else if (CCTK_Equals(initial_data, "random_noise"))
  {
    SimpleMaxwell_RandomNoise(CCTK_PASS_CTOC);
    return;
  }
  else
  {
    CCTK_ERROR("You need to implement this choice");
  }
}

static void SimpleMaxwell_Pulse(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const CCTK_REAL pi = 3.1415926535897932384626433832795;

  double mag_pulse = 1.;

  if (CCTK_Equals(initial_data, "compact_pulse_plus_z"))
  {
    mag_pulse = 1;
  }
  else if (CCTK_Equals(initial_data, "compact_pulse_bidirectional"))
  {
    mag_pulse = 0;
  }

  const double omega = 2 * pi / compact_pulse_wavelength * compact_pulse_width;

  /* Loop over all points (ghostzones included) */
  const int ni = cctkGH->cctk_lsh[0];                   
  const int nj = cctkGH->cctk_lsh[1];
  const int nk = cctkGH->cctk_lsh[2];
# ifdef _OPENMP
#   pragma omp parallel
# endif	
  LC_LOOP3(Initial_Pulse, i, j, k,
       0, 0, 0,
       ni, nj, nk,
       ni, nj, nk)
  {
        const size_t ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);
        const double X_cor = x[ijk];
        const double Y_cor = y[ijk];
        const double Z_cor = z[ijk] / compact_pulse_width;
        double factor = 1.0;
        if (compact_pulse_perp_width > 0)
        {
          const double r2 = X_cor * X_cor + Y_cor * Y_cor;
          factor = exp(-r2 / compact_pulse_perp_width / compact_pulse_perp_width);
        }

        PsiB[ijk] = 0.0;
        PsiD[ijk] = 0.0;

        Dx[ijk] = -sin(omega * Z_cor) * exp(-Z_cor * Z_cor) * factor;
        Dy[ijk] = 0;
        Dz[ijk] = 0;

        Bx[ijk] = 0;
        By[ijk] = (-sin(omega * Z_cor) * exp(-Z_cor * Z_cor)) * mag_pulse * factor;
        Bz[ijk] = 0;
  }
  LC_ENDLOOP3(Initial_Pulse);
}


static void SimpleMaxwell_RandomNoise(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  /* Loop over all points (ghostzones included) */
  const int ni = cctkGH->cctk_lsh[0];                   
  const int nj = cctkGH->cctk_lsh[1];
  const int nk = cctkGH->cctk_lsh[2];
# ifdef _OPENMP
#   pragma omp parallel
# endif	
  LC_LOOP3(Initial_Random, i, j, k,
       0, 0, 0,
       ni, nj, nk,
       ni, nj, nk)
  {
        const size_t ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

        PsiB[ijk] = 2.0 * rand() / (double)RAND_MAX - 1.0;
        PsiD[ijk] = 2.0 * rand() / (double)RAND_MAX - 1.0;

        Dx[ijk] = 2.0 * rand() / (double)RAND_MAX - 1.0;
        Dy[ijk] = 2.0 * rand() / (double)RAND_MAX - 1.0;
        Dz[ijk] = 2.0 * rand() / (double)RAND_MAX - 1.0;

        Bx[ijk] = 2.0 * rand() / (double)RAND_MAX - 1.0;
        By[ijk] = 2.0 * rand() / (double)RAND_MAX - 1.0;
        Bz[ijk] = 2.0 * rand() / (double)RAND_MAX - 1.0;
  }
  LC_ENDLOOP3(Initial_Random);
}

void SimpleMaxwell_SetConstantFields(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (CCTK_Equals(material_properties, "gaussian"))
  {
    SimpleMaxwell_Gaussian(CCTK_PASS_CTOC);
  }
  else if (CCTK_Equals(material_properties, "constant"))
  {
    SimpleMaxwell_ConstantMaterial(CCTK_PASS_CTOC);
  }
  else
  {
    CCTK_ERROR("You need to implement this choice");
  }
}

static void SimpleMaxwell_ConstantMaterial(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  /* Loop over all points (ghostzones included) */
  const int ni = cctkGH->cctk_lsh[0];                   
  const int nj = cctkGH->cctk_lsh[1];
  const int nk = cctkGH->cctk_lsh[2];
# ifdef _OPENMP
#   pragma omp parallel
# endif	
  LC_LOOP3(InitialMaterialConstant, i, j, k,
       0, 0, 0,
       ni, nj, nk,
       ni, nj, nk)
  {
        const size_t ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

        ieps[ijk] = 1.0 / constant_epsilon;
        imu[ijk] = 1.0 / constant_mu;
  }
  LC_ENDLOOP3(InitialMaterialConstant);
}

static void SimpleMaxwell_Gaussian(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  /* Loop over all points (ghostzones included) */
  const int ni = cctkGH->cctk_lsh[0];                   
  const int nj = cctkGH->cctk_lsh[1];
  const int nk = cctkGH->cctk_lsh[2];
# ifdef _OPENMP
#   pragma omp parallel
# endif	
  LC_LOOP3(InitialMaterialGaussian, i, j, k,
       0, 0, 0,
       ni, nj, nk,
       ni, nj, nk)
  {
        const size_t ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);
        const double gx = (x[ijk] - gaussian_x_origin)/gaussian_x_width;
        const double gy = (y[ijk] - gaussian_y_origin)/gaussian_x_width;
        const double gz = (z[ijk] - gaussian_z_origin)/gaussian_x_width;

        const double gauss = exp(-gx*gx - gy * gy - gz * gz);

        

        ieps[ijk] = constant_epsilon + gauss * gaussian_delta_epsilon;
        ieps[ijk] = 1.0 / ieps[ijk];
        imu[ijk] = constant_mu + gauss * gaussian_delta_mu;
        imu[ijk] = 1.0 / imu[ijk];
  }
  LC_ENDLOOP3(InitialMaterialGaussian);
}
