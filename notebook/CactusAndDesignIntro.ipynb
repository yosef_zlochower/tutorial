{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "fafd4544",
   "metadata": {},
   "source": [
    "# Obtaining SimpleMaxwell Thorn\n",
    "Follow the instructions at [here](https://bitbucket.org/yosef_zlochower/tutorial/src/tutorial_2023/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e10995a",
   "metadata": {},
   "source": [
    "# Defining grid functions\n",
    "\n",
    "Suppose we want to define four gridfunction, f, dx_f, dy_f, and dz_f, where the latter three are going to represent the gradient of the first. In our interface.ccl, we would add\n",
    "```\n",
    "CCTK_REAL my_scalar TYPE=GF TIMELEVELS=3 TAGS='tensortypealias=\"Scalar\"'\n",
    "{\n",
    "  f\n",
    "} \"scalar fields\"\n",
    "\n",
    "CCTK_REAL my_gradient TYPE=GF TIMELEVELS=3 TAGS='tensortypealias=\"U\"'\n",
    "{\n",
    "  dx_f, dy_f, dz_f\n",
    "} \"Gradient field\"\n",
    "```\n",
    "\"my_scalar\" and \"my_gradient\" are the group names for these gridfunctions (groups are collections of gridfunction). We need to tell cactus that the first is a scalar and the second a vector so that symmetry boundary conditions can be automatically applied\n",
    "\n",
    "We indicate when storage is allocated for the gridfunctions in the schedule.ccl file, for example\n",
    "```\n",
    "STORAGE: my_scalar[3]\n",
    "STORAGE: my_gradient[3]\n",
    "```\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a9db28c5",
   "metadata": {},
   "source": [
    "## Using gridfunctions: Derivatives\n",
    "Cactus stores 3-dimensional arrays as 1-dimensional arrays in Fortran ordering. The logical dimension of gridfunctions are stored in the array, cctk_lsh.\n",
    "\n",
    "|Array Index | Logical Index (i, j, k)\n",
    "| --- | --- |\n",
    "| 0 | (0,0,0) |\n",
    "| 1 | (1,0,0) |\n",
    "| 2 | (2,0,0) |\n",
    "| ... | ...|\n",
    "| nx-1 | (nx -1, 0,0)|\n",
    "| padding|\n",
    "| nx + padding| (0,1,0) |\n",
    "| ... | ... |\n",
    "| | (nx-1, ny-1, 0)|\n",
    "| padding |\n",
    "| | (0,0, 1)|\n",
    "\n",
    "To relate the logical indices (i,j,k) with the array index, use the Macro <code> CCTK_GFINDEX3D(cctkGH, i, j, k)</code>, i.e.,\n",
    "<code>val = array[CCTK_GFINDEX3D(cctkGH, i, j, k)]</code>\n",
    "\n",
    "We can therefore use the following code to calculate derivative, assuming that f, dx_f, dy_f, and dz_f are gridfunctions\n",
    "\n",
    "```C\n",
    "/* grid spacing in x */\n",
    "const CCTK_REAL dx = CCTK_DELTA_SPACE(0);\n",
    "/* grid spacing in x */\n",
    "const CCTK_REAL dy = CCTK_DELTA_SPACE(1);\n",
    "/* grid spacing in x */\n",
    "const CCTK_REAL dz = CCTK_DELTA_SPACE(2);\n",
    "\n",
    "for (int k = 1; k < cctk_lsh[2] - 1; k++)\n",
    "{\n",
    "  for (int j = 1; j < cctk_lsh[1] - 1; j++)\n",
    "  {\n",
    "    for (int i = 1; i < cctk_lsh[0] - 1; i++)\n",
    "    {\n",
    "      /* Index of central point and neighbors */\n",
    "      const CCTK_INT ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);\n",
    "      \n",
    "      // +- x neighbors\n",
    "      const CCTK_INT ip1jk = CCTK_GFINDEX3D(cctkGH, i + 1, j, k);\n",
    "      const CCTK_INT im1jk = CCTK_GFINDEX3D(cctkGH, i - 1, j, k);\n",
    "      \n",
    "      // +- y neighbors\n",
    "      const CCTK_INT ijp1k = CCTK_GFINDEX3D(cctkGH, i, j + 1, k);\n",
    "      const CCTK_INT ijm1k = CCTK_GFINDEX3D(cctkGH, i, j - 1, k);\n",
    "      \n",
    "      // +- z neighbors\n",
    "      const CCTK_INT ijkp1 = CCTK_GFINDEX3D(cctkGH, i, j, k + 1);\n",
    "      const CCTK_INT ijkm1 = CCTK_GFINDEX3D(cctkGH, i, j, k - 1);\n",
    "      \n",
    "      dx_f[ijk] = 0.5 * (f[ip1jk] - f[im1jk])/dx;\n",
    "      dy_f[ijk] = 0.5 * (f[ijp1k] - f[ijm1k])/dy;\n",
    "      dz_f[ijk] = 0.5 * (f[ijkp1] - f[ijkm1])/dz;\n",
    "    }\n",
    "  }\n",
    "}\n",
    "```\n",
    "Note that the loop bounds skip the first and last elements in all directions."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2b015cb2",
   "metadata": {},
   "source": [
    "# Design Choices\n",
    "\n",
    "* No external boundary conditions are implemented (use periodic BCs).\n",
    "* The fields $\\rho$ and $\\vec J$ are assumed zero.\n",
    "* Only second-order finite differencing is used.\n",
    "\n",
    "All of these choices can be modified. See, e.g., the code in the 'tutorial_2021' branch\n",
    "\n",
    "* The evolution routine itself is coded using a very _naive_ python script.\n",
    "Use NRPY+ or similar for a production code."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "66c1bb4f",
   "metadata": {},
   "source": [
    "# Appendix: Cactus variables and Macros Used in SimpleMaxwell\n",
    "\n",
    "| symbol | type | use / meaning |\n",
    "| --- | --- | --- |\n",
    "| CCTK_ARGUMENTS | macro | argument list for functions called by cactus |\n",
    "| cctk_lsh[3] | array of ints | local grid dimensions excluding padding |\n",
    "| cctk_ash[3] | array of ints | local grid dimensions including padding |\n",
    "| cctk_bbox[6] | array of ints | is local grid boundary the domain boundary |\n",
    "| CCTK_DELTA_SPACE() | macro takes int as input| grid spacing per direction |\n",
    "| CCTK_DELTA_TIME | macro | time step size |\n",
    "| DECLARE_CCTK_ARGUMENTS | macro | declares GFs and several useful cactus variables |\n",
    "| DECLARE_CCTK_PARAMETERS | macro | declares parameters |\n",
    "| CCTK_REAL | type | default real type (typically double)|\n",
    "| CCTK_INT | type | default int type (typically int)|\n",
    "| CCTK_GFINDEX3D() | macro | converts logical i,j,k indices to flat array index |\n",
    "| CCTK_VarIndex | function | determines GF ID based on its name |\n",
    "| MoLRegisterEvolved | function | registers a GF to be evolved with MoL |\n",
    "| MoLRegisterConstrained | function | registers a GF as a constrained variable with MoL |\n",
    "| CCTK_WARN | wrapper to function | emit a warning message |\n",
    "| CCTK_WARN_ABORT | macro | argument to CCTK_WARN, causes program termination|\n",
    "| CCTK_WARN_ALERT | macro | argument to CCTK_WARN, indicates warning is not fatal|\n",
    "| CCTK_INFO | wrapper to function | emit an informational message |\n",
    "| Boundary_SelectGroupForBC | function | Register a group of GFs for boundary conditions the next time ApplyBCs is called\n",
    "\n",
    "\n",
    "The boundaries of the grid on any given processor may or may not be part of the domain boundary. If you algorithm changes near the domain boundaries, then you can use cctk_bbox[] to determine which of local boundaries are part of the domain boundaries.\n",
    "\n",
    "If `cctk_bbox[0] == 1` then the lower x boundary is part of the domain boundary. <br> \n",
    "If `cctk_bbox[1] == 1` then the upper x boundary is part of the domain boundary. <br> \n",
    "If `cctk_bbox[2] == 1` then the lower y boundary is part of the domain boundary. <br> \n",
    "If `cctk_bbox[3] == 1` then the upper y boundary is part of the domain boundary. <br> \n",
    "If `cctk_bbox[4] == 1` then the lower z boundary is part of the domain boundary. <br> \n",
    "If `cctk_bbox[5] == 1` then the upper z boundary is part of the domain boundary. <br>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7bd50f2",
   "metadata": {},
   "source": [
    "# Appendix: Scheduling bins and groups used in SimpleMaxwell\n",
    "\n",
    "Note that schedule groups are scheduled inside of schedule bins. Routines or groups are scheduled *at* bins, while routines or other groups are schedule *in* a group.\n",
    "\n",
    "`CCTK_BASEGRID`: Scheduling bin for setting up coordinates and perforing initializations that need to be done each time a simulation restart.<br> \n",
    "`CCTK_INITIAL`: Scheduling bin for initial data. Routines scheduled here only run at the start of a simulation (iteration 0).<br>\n",
    "\n",
    "`CCTK_POSTRESTRICTINITIAL`: Scheduling bin for routines that need to be run after a _restriction_ operation at the initial time step (iteration 0). Only relevant if mesh refinement is used.\n",
    "<br>\n",
    "`CCTK_POSTRESTRICT`: Scheduling bin for routines that need to be run after a _restriction_ operation after iteration 0. Only relevant if mesh refinement is used.\n",
    "<br>\n",
    "`CCTK_POSTREGRIDINITIAL`: Scheduling bin for routines that need to be run after a _regridding_ operation during iteration 0. Only relevant if mesh refinement is used.\n",
    "<br>\n",
    "`CCTK_POSTREGRIDINITIAL`: Scheduling bin for routines that need to be run after a _regridding_ operation after iteration 0. Only relevant if mesh refinement is used.\n",
    "<br>\n",
    "<br>\n",
    "`MoL_CalcRHS`: A schedule group for routines that fill in RHS variables.\n",
    "<br>\n",
    "`MoL_PostStep`: A schedule group for routines that need to run after a MoL ministep completes. Examples would be routines implement algebraic constraints in BSSN.\n",
    "<br>\n",
    "`MoL_PseudoEvolution`: A schedule group for routines that update constrained variables.\n",
    "\n",
    "Consider\n",
    "```\n",
    "SCHEDULE GROUP SimpleMaxwell_PostMiniStep IN MoL_PostStep\n",
    "{                                           \n",
    "} \"Schedule group for symmetries and others\"\n",
    "\n",
    "\n",
    "SCHEDULE SimpleMaxwell_Boundaries IN SimpleMaxwell_PostMiniStep\n",
    "{               \n",
    "  OPTIONS: LEVEL                                      \n",
    "  SYNC: EvolvedVectorD, EvolvedVectorB, EvolvedScalars\n",
    "  LANG: C                                \n",
    "} \"Schedule symmetry boundary conditions\"\n",
    "\n",
    "```\n",
    "The function SimpleMaxwell_Boundaries is being scheduled within the group SimpleMaxwell_PostMiniStep, which, in turn, is scheduled within the group MoL_PostStep, which, in turn (although not apparent from the above) is ultimately scheduled *at* several diffrent CCTK bins."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "69acb030",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": false,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
